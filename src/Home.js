import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image,ScrollView, ImageBackground, Dimensions } from 'react-native';
import { Header, Icon, Card } from 'react-native-elements';
import {LineChart} from 'react-native-chart-kit';

export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
                    <Icon
                        color='white'
                        name='menu' />
                </TouchableOpacity>
            </View>
        );
    }
    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={s.title}>HOME</Text>
            </View>
        );
    }


    render() {
        const data = {
            labels: ['Park B10', 'Park B4', 'Food C6', 'Food B4', 'Lab C5'],
            datasets: [{
                data: [20, 45, 28, 51, 60]
            }]
        }
        return (
            <View style={{flex:1}}>
                <Header
                    leftComponent={this.renderMenu()}
                    centerComponent={this.renderCenter()}
                    outerContainerStyles={{ backgroundColor: '#445870' }}
                />
                <ScrollView>
                <View style={{ backgroundColor: '#f8f8f9' }}>
                    <Card containerStyle={{ backgroundColor: '#9900cc', elevation: 1, borderRadius: 10 }}>
                        {/* <View style={{marginTop: 30, backgroundColor:'#5bb5ff'}}> */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('User details')}>
                            <Text style={{ fontWeight: 'bold',fontSize:20,color:'#f7f7f8' }}>Nguyen Song Hung Anh</Text>
                            <Text style={{color:'#f7f7f8'}}>1552025@hcmut.edu.vn</Text>
                            <Text style={{color:'#f7f7f8'}}>012345678</Text>
                            <View style={s.more}>
                                <Text style={{ fontWeight: 'bold' }}>More details ></Text>
                            </View>
                        </TouchableOpacity>
                        {/* </View> */}
                    </Card>
                    <Card containerStyle={{ backgroundColor: 'rgba(2,194,211, 1)', borderRadius: 10 }}>
                        <Text style={{ alignSelf: 'flex-start', fontWeight: 'bold' }}>Balance:</Text>
                        <Text style={{ alignSelf: 'flex-end', fontWeight: 'bold', fontSize: 30 }}>150 000đ</Text>
                    </Card>
                    <Card containerStyle={{ borderRadius: 10, backgroundColor:'rgba(192,192,192,1)' }}>
                        <Text style={{ fontWeight: 'bold' }}>My activity in last 7 days</Text>
                        <LineChart
                            style={s.line}
                            data={data}
                            width={350}
                            height={220}
                            chartConfig={{
                                backgroundColor: '#e26a00',
                                backgroundGradientFrom: '#77a3f9',
                                backgroundGradientTo: '#51877c',
                                decimalPlaces: 0, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                style: {
                                    borderRadius: 16
                                }

                            }}
                        />
                    </Card>
                </View>
                </ScrollView>
            </View>
        );
    }
}

const s = StyleSheet.create({
    more: {
        justifyContent: 'center',
        alignSelf: 'flex-end',
        alignItems: 'center',
        backgroundColor: 'red',
        width: 100,
        height: 30,
        borderRadius: 10
    },
    line: {
        marginVertical: 8,
        marginTop: 15,
        alignSelf: 'center',
        borderRadius: 16
    },
    title: {
        marginLeft: 10,
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white'
    }
})