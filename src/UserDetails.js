import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ImageBackground,Dimensions } from 'react-native';
import { Header, Icon, Card } from 'react-native-elements';

export default class UserDetails extends Component {
    constructor(props) {
        super(props);
    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                    <Icon
                        color='white'
                        name='arrow-back' />
                </TouchableOpacity>
            </View>
        );
    }
    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 20, color: 'white' }}>INFORMATION</Text>
            </View>
        );
    }

    render() {
        return (
            <View>
                <Header
                    leftComponent={this.renderMenu()}
                    centerComponent={this.renderCenter()}
                    outerContainerStyles={{ backgroundColor: '#445870' }}
                />
            </View>
        );
    }
}