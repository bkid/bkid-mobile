import React, { Component } from 'react';
import { StyleSheet, View, Text, Stylesheet, TouchableOpacity, Picker, Image, ImageBackground, FlatList } from 'react-native';
import { Header, Icon, Card, SearchBar } from 'react-native-elements';
import Axios from 'axios';
import Modal from "react-native-modal";

export default class Transaction extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            search: '',
            refreshing: false,
            modalVisible: false,
            language: '',
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    updateSearch = search => {
        this.setState({ search });
    };

    onLoadData = () => {
        this.setState({ refreshing: true })
        Axios.get("http://bk-id.herokuapp.com/users")
            .then((res) => {
                if (res.status == 200) {
                    let response = []
                    res.data.map((item, index) => {
                        if (item.studentID) {
                            response.push(item)
                        }
                    })
                    this.setState({ data: response, refreshing: false })
                }
            })
    }

    componentDidMount = () => {
        this.onLoadData()
    }

    renderMenu = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
                    <Icon
                        color='white'
                        name='menu' />
                </TouchableOpacity>
            </View>
        );
    }

    renderCenter = () => {
        return (
            <View style={{ flexDirection: "row" }}>
                <Image style={{ width: 30, height: 30 }} source={require('./assets/Bk.png')} />
                <Text style={{ marginLeft: 10, fontWeight: 'bold', fontSize: 20, color: 'white' }}>TRANSACTION</Text>
            </View>
        );
    }

    renderRight = () => {
        return (
            <View>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Filter')}>
                    <Image style={{ width: 30, height: 30, tintColor: 'white' }} source={require('./assets/filter.png')} />
                </TouchableOpacity>
            </View>
        )
    }

    renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('Detail', item.UID) }}>
                <Card style={{ width: '100', backgroundColor: 'white', flexDirection: 'column' }}>
                    <Text>ID: {item.studentID}</Text>
                    <Text>Name: {item.name}</Text>
                    <Text>UID: {item.UID}</Text>
                </Card>
            </TouchableOpacity>
        )
    }

    render() {
        const { search } = this.state;
        const { data } = this.state;
        let picker;

        if (this.state.modalVisible) {
            picker =
                <View style={{justifyContent:'center',alignItems:'center',alignSelf:'center',borderWidth:0.2,borderRadius: 10,}}>
                    <Picker
                        selectedValue={this.state.language}
                        style={{ height: 30, width: 90, }}
                        onValueChange={(itemValue, itemIndex) =>
                            this.setState({ language: itemValue })
                        }>
                        <Picker.Item label="A -> Z" value="A" />
                        <Picker.Item label="Z -> A" value="Z" />
                        <Picker.Item label="Up" value="up"/>
                        <Picker.Item label="Down" value="down"/>
                    </Picker>
                </View>;
        }

        if (!data) {
            return (
                <View>
                    <Text>
                        Waiting for data
                    </Text>
                </View>
            )
        }
        else {
            return (
                <View style={{ flex: 1 }}>
                    <Header
                        leftComponent={this.renderMenu()}
                        centerComponent={this.renderCenter()}
                        // rightComponent={this.renderRight()}
                        outerContainerStyles={{ backgroundColor: '#445870' }}
                    />

                    <View style={{ flexDirection: 'row' }}>
                        <SearchBar
                            placeholder="Type Here To Search"
                            onChangeText={this.updateSearch}
                            lightTheme
                            round
                            inputStyle={{ backgroundColor: 'white' }}
                            value={search}
                            containerStyle={{ width: 340 }}
                        />
                        <TouchableOpacity style={{ justifyContent: 'center', alignContent: 'center', }} onPress={() => this.props.navigation.navigate('Filter')}>
                            <Icon
                                color='black'
                                reverse
                                containerStyle={{ height: 40, width: 40, marginLeft: 5, backgroundColor: 'green' }}
                                name='format-list-bulleted' />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        onPress={() => { this.setModalVisible(!this.state.modalVisible); }}>
                        <View style={s.sort}>
                            <Text style={{fontWeight:'bold'}}>Sort </Text>
                            <Icon

                                color='black'
                                name='sort' />
                        </View>
                    </TouchableOpacity>

                    {picker}
                    
                    <FlatList
                        data={data}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => item._id}
                        ListFooterComponent={<View style={{ marginBottom: 40, }} />}
                        ListEmptyComponent={
                            <View>
                                <Text style={{ textAlign: 'center' }}>There is no data</Text>
                            </View>
                        }
                        refreshing={this.state.refreshing}
                        onRefresh={this.onLoadData}
                    />
                </View>
            );
        }
    }
}

const s = StyleSheet.create({
    modal: {
        alignSelf: 'center',
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        height: 80,
    },
    sort: {
        marginLeft: 10,
        flexDirection: 'row',
        width: 60,
        justifyContent: 'space-between',
        borderLeftWidth: 0.2,
        borderRightWidth: 0.2,
        borderBottomWidth: 0.2,
        alignItems: 'center',
        alignSelf: 'center'
    }
})